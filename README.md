# Go Init
A simple CLI tool for setting your go module name to your git repo.

## Installation
There are two options:

1. Install the binary into `$(go env GOPATH)/bin`
```bash
go install gitlab.com/davidjmacdonald1/go-init@latest
```
2. Clone this repository and build manually
```bash
git clone git@gitlab.com:davidjmacdonald1/go-init.git
cd go-init
go build -o go-init
```

## Usage
This tool can be used to initialize a new local git repo, or update an existing
one.

Let `[REPO]` represent the mod name.
This will automatically be parsed from the `[URL]` (SSH or HTTPS).

### Init
```bash
go-init [URL]
```
It will run `git init`, `git remote add origin [URL]`, and `go mod init [REPO]`.

### Update
```bash
go-init --replace [URL]
```

First, it will grab the old `[REPO]` from the existing go mod file; call it `[OLD_REPO]`.
It will run `git remote set-url origin [URL]` and `go mod edit -module [REPO]`.
Then, it will use sed to replace all existing references of `[OLD_REPO]` with `[REPO]`.
This will occur in all `.go` files and all `.templ` files in the current directory.
