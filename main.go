package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

var debug bool

func main() {
	replaceFlag := flag.Bool("replace", false, "replace an existing url")
	flag.BoolVar(&debug, "debug", false, "print the executed bash commands")
	flag.Parse()

	link := flag.Arg(0)
	if link == "" {
		exit("Must provide one argument: a git clone link")
	}

	if *replaceFlag {
		replace(link)
	} else {
		createNew(link)
	}
}

func replace(link string) {
	if _, err := os.Stat(".git"); os.IsNotExist(err) {
		exit("Git repo does not exist yet")
	}

	repo := linkToRepo(link)
	currentRepo := readCurrentRepo()
	sedSeq := fmt.Sprintf("s/%s/%s/g", escape(currentRepo), escape(repo))

	mustRun("git", "remote", "set-url", "origin", link)
	mustRun("go", "mod", "edit", "-module", repo)
	mustRun("find", ".", "-type", "f", "-name", "*.go", "-exec",
		"sed", "-i", sedSeq, "{}", ";")
	mustRun("find", ".", "-type", "f", "-name", "*.templ", "-exec",
		"sed", "-i", sedSeq, "{}", ";")

	fmt.Println("Replacing", currentRepo, "with", repo)
}

func createNew(link string) {
	if _, err := os.Stat(".git"); err == nil {
		exit("Git repo already exists")
	}

	repo := linkToRepo(link)
	mustRun("git", "init")
	mustRun("git", "remote", "add", "origin", link)
	mustRun("go", "mod", "init", repo)
	fmt.Println("Initialized git origin and go mod to", repo)
}

func readCurrentRepo() string {
	file, err := os.ReadFile("go.mod")
	if err != nil {
		exit(err)
	}

	lines := strings.Split(string(file), "\n")
	line := strings.Replace(lines[0], "module", "", 1)
	return strings.TrimSpace(line)
}

func linkToRepo(link string) string {
	repo := strings.Replace(link, ".git", "", 1)
	isHttps := regexp.MustCompile(`^https://.*$`).MatchString(link)
	if isHttps {
		repo = strings.Replace(repo, "https://", "", 1)
	} else {
		repo = strings.Replace(repo, "git@", "", 1)
		repo = strings.Replace(repo, ":", "/", 1)
	}

	return repo
}

func escape(repo string) string {
	return strings.ReplaceAll(repo, `/`, `\/`)
}

func mustRun(command string, args ...string) {
	cmd := exec.Command(command, args...)
	if debug {
		fmt.Println(cmd.String())
	}

	err := cmd.Run()
	if err != nil {
		exit(err)
	}
}

func exit(a ...any) {
	fmt.Println(a...)
	os.Exit(1)
}
